public class MethodsTest {
	
	public static void main(String[] args) {
		
	int x = 10;
	
	System.out.println(x);
	
	methodNoInputReturn();
	
	System.out.println(x);
	
	methodOneInputNoReturn(10);
	methodOneInputNoReturn(x);
	methodOneInputNoReturn(x+50);
	
	methodTwoInputNoReturn();
	
	int z = methodNoInputReturnInt();
	
	System.out.println(z);
	
	double y = sumSquareRoot(6,3);
		
	System.out.println(y);
	
	String s1 = "hello";
	String s2 = "goodbye";
	
	System.out.println(s1.length());
	System.out.println(s2.length());
	
	SecondClass.addOne(50);
	SecondClass.addTwo(50);
	
	SecondClass sc = new SecondClass();
	
	System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputReturn() {
		
		System.out.println("I'm a method that takes no input and returns nothing");
		
		int x = 50;
		
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int var) {
		
		System.out.println("Inside the method one input no return " + var);
	}
	
	public static void methodTwoInputNoReturn(int var, double var2) { 		 	
	
	
	}
	
	public static int methodNoInputReturnInt() {
		
		return 6;
	}
	
	public static double sumSquareRoot(int var, int var2) {
		
		double total = var + var2;
		
		return Math.sqrt(total);
	}
		
}