import java.util.Scanner;
public class PartThree {
	
	public static void main(String[] args) {
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter the side's length of the square: ");
		double side = reader.nextDouble();
		System.out.print("Enter the rectangle's length: ");
		double length = reader.nextDouble();
		System.out.print("Enter the rectangle's width: ");
		double width = reader.nextDouble();
		
		AreaComputations calculator = new AreaComputations();
		
		double rectangleArea = calculator.AreaRectangle(length,width);
		double squareArea = AreaComputations.areaSquare(side);
		
		System.out.println("The area of the rectangle is: " + rectangleArea);
		System.out.println("The area of the square is: " + squareArea);
	}
	
}